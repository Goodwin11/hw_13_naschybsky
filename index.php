<?php 
require_once $_SERVER['DOCUMENT_ROOT'] . '/database/connect.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/data.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/administrator.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/student.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/teacher.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/seeder_db.php';


try{    
    $sql = "SELECT * FROM members;";
    $statement  = $db->query($sql);
    $data = $statement->fetchAll();
}catch(Exception $exception){
    die('error ' . $exception->getMessage());
};


$objects = [];
if($data){
    foreach($data as $person){
        switch($person['role']){
            case 'student':
                $objects[] = new Student(
                    $person['full_name'],
                    $person['phone'],
                    $person['email'],
                    $person['role'],
                    $person['averange_mark']
                );
            break;
            case 'teacher':
                $objects[] = new Teacher(
                    $person['full_name'],
                    $person['phone'],
                    $person['email'],
                    $person['role'],
                    $person['subject_'] 
                );
            break;
            case 'administrator':
                $objects[] = new Administrator(
                    $person['full_name'],
                    $person['phone'],
                    $person['email'],
                    $person['role'],
                    $person['working_day'] 
                );
            break;

        }
    }
}

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-wEmeIV1mKuiNpC+IOBjI7aAzPcEZeedi5yW5f2yOq55WWLwNGmvvx4Um1vskeMj0" crossorigin="anonymous">
    <meta name="Description" content="CreateMyDb">
    <title>CreateMyDb</title>
</head>
<body>
    <ul class="list-group">
        <?php foreach($objects as $key => $object): ?>
            <li class="list-group-item"><?= $object->getVisitCard(); ?></li>
        <?php endforeach; ?>
    </ul>
</body>
</html>