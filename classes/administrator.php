<?php

class Administrator extends Person {
    public $workingDay;
    public function __construct($fullName, $phone, $email, $role, $workingDay){
        parent::__construct($fullName, $phone, $email, $role);
        $this->workingDay = $workingDay;
    }

    public function getVisitCard(){
       return parent::getVisitCard(). ', Working day: ' . $this->workingDay;
    }

}

?>