<?php

class Person {
    public $fullName;
    public $phone;
    public $email;
    public $role;

    public function __construct($fullName, $phone, $email, $role) {
        $this->fullName = $fullName;
        $this->phone = $phone;
        $this->email = $email;
        $this->role = $role;
    }

    public function getVisitCard(){
        return 'full name: '. $this->fullName .
        ', phone: ' . $this->phone . 
        ', email: ' . $this->email . 
        ', role: ' . $this->role;
    }
}

?>