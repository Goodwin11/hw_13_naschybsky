<?php

class Teacher extends Person {
    public $subject;
    public function __construct($fullName, $phone, $email, $role, $subject){
        parent::__construct($fullName, $phone, $email, $role);
        $this->subject = $subject;
    }

    public function getVisitCard(){
       return parent::getVisitCard(). ', Subject: ' . $this->subject;
    }

}

?>