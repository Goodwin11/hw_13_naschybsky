<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/database/connect.php';
$seedEntries = [
    [
        'full_name' => 'John Norton',
        'phone' => '0946565656',
        'email' => 'Norton@gmail.com',
        'role' => 'student',
        'averange_mark' => 4.5
    ],
    [
        'full_name' => 'Ben Simons',
        'phone' => '0935557687',
        'email' => 'Simons13@gmail.com',
        'role' => 'student',
        'averange_mark' => 4.2
    ],
    [
        'full_name' => 'Nike Sun',
        'phone' => '0935985634',
        'email' => 'Sun32@gmail.com',
        'role' => 'administrator',
        'working_day' => 'monday'
    ],
    [
        'full_name' => 'Jasmin Smith',
        'phone' => '0932599065',
        'email' => 'Sity22@gmail.com',
        'role' => 'administrator',
        'working_day' => 'tuesday'
    ],
    [
        'full_name' => 'Lola Holms',
        'phone' => '0973445645',
        'email' => 'killa22@gmail.com',
        'role' => 'administrator',
        'working_day' => 'friday'
    ],
    [
        'full_name' => 'Sara Parker',
        'phone' => '0936677056',
        'email' => 'Spider22@gmail.com',
        'role' => 'student',
        'averange_mark' => 3.9
    ],
    [
        'full_name' => 'Nina Wiliams',
        'phone' => '0935556677',
        'email' => 'FoxyOnFire@gmail.com',
        'role' => 'teacher',
        'subject' => 'English'
    ],
    [
        'full_name' => 'Scarlet Kool',
        'phone' => '0934445465',
        'email' => 'rose@gmail.com',
        'role' => 'teacher',
        'subject' => 'German'
    ],
];
// в зависимости от роли, добавляется уникальное свойство
foreach($seedEntries as $data) {
    if($data['role'] == 'student') {
        $averange_mark = $data['averange_mark'];
    } elseif($data['role'] == 'teacher') {
        $subject = $data['subject'];
    } elseif($data['role'] == 'administrator') {
        $working_day = $data['working_day'];
    }
    
    $sql = "INSERT INTO members SET
            full_name ='". $data['full_name'] ."',
            phone='{$data['phone']}',
            email='{$data['email']}',
            role ='". $data['role'] ."',
            averange_mark = '". $averange_mark ."',
            subject = '". $subject ."',
            working_day = '". $working_day ."'";
    $db->exec($sql);
}
echo 'Data added';
?>

